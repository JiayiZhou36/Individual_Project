# Cloud Individual Project 1: Personal Website
[![pipeline status](https://gitlab.com/JiayiZhou36/Individual_Project/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/Individual_Project/-/commits/main)

### Video Demonstration
[YouTube](https://youtu.be/yubzPr2EBhI)

### Website
[Portfolio Website](https://jiayizhou.vercel.app/)
![Screenshot_2024-03-07_at_5.41.15_PM](/uploads/6a6ca4e340996c7297ae3b33d2e47b39/Screenshot_2024-03-07_at_5.41.15_PM.png)

### Purpose
This project involves the creation of a static personal website utilizing Zola as the framework and hosted on Vercel. By employing Zola, a powerful static site generator, and leveraging the hosting capabilities of Vercel, the website is designed to offer a seamless and efficient online presence. Through this initiative, the aim is to craft a polished and professional platform that showcases personal content and past projects in a visually appealing manner.

### Preparation
1. Install Zola 
2. Create website with `zola init`
3. Add a theme for css `cd themes` then `git submodule add <theme name>`
4. Create pages in the `content` folder 
5. `zola serve` to test website or `zola build`
6. Install Vercel CLI
7. Login to Vercel with `vercel login`
8. Link your repo with `vercel link`
9. Copy your Vercel token, Vercel team id, and Vercel Org id to Gitlab secrets 
10. Push repo to run pipeline filr `.gitlab-ci.yml`